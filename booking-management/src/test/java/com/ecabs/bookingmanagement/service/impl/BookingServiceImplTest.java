package com.ecabs.bookingmanagement.service.impl;

import com.ecabs.bookingmanagement.repository.BookingRepository;
import com.ecabs.bookingmanagement.service.BookingProducerService;
import com.ecabs.common.dto.BookingDTO;
import com.ecabs.common.model.Booking;

import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class BookingServiceImplTest {
    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private BookingProducerService bookingProducerService;

    @InjectMocks
    private BookingServiceImpl bookingService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        bookingService = new BookingServiceImpl(bookingRepository, bookingProducerService);
    }

    @Test
    void shouldCreateBooking() {
        BookingDTO bookingDTO = new BookingDTO();
        doNothing().when(bookingProducerService).sendBookingAddMessage(bookingDTO);
        BookingDTO result = bookingService.createBooking(bookingDTO);
        assertEquals(bookingDTO, result);
        verify(bookingProducerService, times(1)).sendBookingAddMessage(bookingDTO);
    }

    @Test
    void shouldUpdateBooking() {
        BookingDTO updatedBookingDTO = new BookingDTO();
        updatedBookingDTO.setId(1L);

        doNothing().when(bookingProducerService).sendBookingEditMessage(updatedBookingDTO);

        BookingDTO result = bookingService.updateBooking(updatedBookingDTO);
        assertEquals(updatedBookingDTO, result);
        verify(bookingProducerService, times(1)).sendBookingEditMessage(updatedBookingDTO);
    }

    @Test
    void shouldDeleteBooking() {
        Long bookingId = 1L;
        bookingService.deleteBooking(bookingId);
        verify(bookingProducerService, times(1)).sendBookingDeleteMessage(bookingId);
    }

    @Test
    void shouldGetBookingById() {
        Long bookingId = 1L;
        Booking booking = new Booking();
        when(bookingRepository.findById(bookingId)).thenReturn(Optional.of(booking));

        Booking result = bookingService.getBooking(bookingId);

        assertEquals(booking, result);
        verify(bookingRepository, times(1)).findById(bookingId);
    }

    @Test
    void shouldThrowExceptionIfBookingNotFound() {
        Long bookingId = 1L;
        when(bookingRepository.findById(bookingId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> bookingService.getBooking(bookingId));

        verify(bookingRepository, times(1)).findById(bookingId);
    }

    @Test
    void shouldGetAllBookings() {
        List<Booking> bookingList = new ArrayList<>();
        when(bookingRepository.findAll()).thenReturn(bookingList);

        List<Booking> result = bookingService.getAllBookings();

        assertEquals(bookingList, result);
        verify(bookingRepository, times(1)).findAll();
    }
}