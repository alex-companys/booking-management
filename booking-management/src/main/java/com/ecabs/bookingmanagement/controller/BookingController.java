package com.ecabs.bookingmanagement.controller;


import com.ecabs.bookingmanagement.service.BookingService;
import com.ecabs.common.dto.BookingDTO;
import com.ecabs.common.model.Booking;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class handling REST API endpoints for managing bookings.
 * Provides endpoints for creating, updating, deleting, and retrieving booking details.
 * All endpoints operate on the '/api/bookings' path.
 *
 * The class uses the BookingService for business logic and interacts with the Booking entity.
 *
 * Endpoints:
 * - POST '/api/bookings': Create a new booking.
 * - PUT '/api/bookings/{id}': Update an existing booking.
 * - DELETE '/api/bookings/{id}': Delete a booking by its ID.
 * - GET '/api/bookings/{id}': Retrieve details of a specific booking by its ID.
 * - GET '/api/bookings': Retrieve details of all bookings.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/bookings")
public class BookingController {

    private final BookingService bookingService;

    @PostMapping
    public ResponseEntity<BookingDTO> createBooking(@RequestBody BookingDTO bookingDTO) {
        BookingDTO createdBookingDTO = bookingService.createBooking(bookingDTO);
        return new ResponseEntity<>(createdBookingDTO, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<BookingDTO> updateBooking(@RequestBody BookingDTO updatedBookingDTO) {
        BookingDTO updated = bookingService.updateBooking(updatedBookingDTO);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBooking(@PathVariable Long id) {
        bookingService.deleteBooking(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Booking> getBooking(@PathVariable Long id) {
        Booking booking = bookingService.getBooking(id);
        return new ResponseEntity<>(booking, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Booking>> getAllBookings() {
        List<Booking> bookings = bookingService.getAllBookings();
        return new ResponseEntity<>(bookings, HttpStatus.OK);
    }

}
