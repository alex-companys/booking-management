package com.ecabs.bookingmanagement.service.impl;


import com.ecabs.bookingmanagement.repository.BookingRepository;
import com.ecabs.bookingmanagement.service.BookingProducerService;
import com.ecabs.bookingmanagement.service.BookingService;
import com.ecabs.common.dto.BookingDTO;
import com.ecabs.common.model.Booking;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service implementation class for managing bookings. Implements the BookingService interface.
 * This class provides methods for creating, updating, deleting, and retrieving booking details.
 * <p>
 * The class interacts with the Booking entity and RabbitMQ for message publishing.
 * It uses the BookingRepository for database operations.
 * <p>
 * Methods:
 * - createBooking: publishes messages to RabbitMQ for auditing and processing Creates a new booking,.
 * - updateBooking: publishes messages to RabbitMQ for auditing and processing Updates an existing booking.
 * - deleteBooking: publishes messages to RabbitMQ for auditing and processing  Deletes a booking by its ID.
 * - getBooking: Retrieves details of a specific booking by its ID.
 * - getAllBookings: Retrieves details of all bookings.
 */
@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final BookingProducerService bookingProducerService;

    @Override
    public BookingDTO createBooking(BookingDTO bookingDTO) {
        bookingProducerService.sendBookingAddMessage(bookingDTO);
        return bookingDTO;
    }

    @Override
    public BookingDTO updateBooking(BookingDTO updatedBookingDTO) {
        bookingProducerService.sendBookingEditMessage(updatedBookingDTO);
        return updatedBookingDTO;
    }

    @Override
    public void deleteBooking(Long bookingId) {
        bookingProducerService.sendBookingDeleteMessage(bookingId);
    }

    @Override
    public Booking getBooking(Long bookingId) {
        return bookingRepository.findById(bookingId)
                .orElseThrow(() -> new EntityNotFoundException("Booking not found with ID: " + bookingId));
    }

    @Override
    public List<Booking> getAllBookings() {
        return bookingRepository.findAll();
    }
}
