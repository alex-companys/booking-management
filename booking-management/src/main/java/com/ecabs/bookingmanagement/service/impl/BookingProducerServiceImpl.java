package com.ecabs.bookingmanagement.service.impl;

import com.ecabs.bookingmanagement.service.BookingProducerService;
import com.ecabs.common.dto.BookingDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Service implementation for producing messages related to booking operations and sending them to RabbitMQ.
 */

@Service
@RequiredArgsConstructor
public class BookingProducerServiceImpl implements BookingProducerService {

    private final RabbitTemplate rabbitTemplate;

    @Value("${rabbitmq.exchange.message}")
    private String messageExchange;

    @Value("${rabbitmq.routingkey.add}")
    private String bookingAddRoutingKey;

    @Value("${rabbitmq.routingkey.edit}")
    private String bookingEditRoutingKey;

    @Value("${rabbitmq.routingkey.delete}")
    private String bookingDeleteRoutingKey;

    @Override
    public void sendBookingAddMessage(BookingDTO bookingDTO) {
        sendMessage(bookingDTO, bookingAddRoutingKey);
    }

    @Override
    public void sendBookingEditMessage(BookingDTO bookingDTO) {
        sendMessage(bookingDTO, bookingEditRoutingKey);
    }

    @Override
    public void sendBookingDeleteMessage(Long bookingId) {
        rabbitTemplate.convertAndSend(messageExchange, bookingDeleteRoutingKey, bookingId);
    }

    private void sendMessage(BookingDTO bookingDTO, String routingKey) {
        rabbitTemplate.convertAndSend(messageExchange, routingKey, bookingDTO);
    }

}
