package com.ecabs.bookingmanagement.service;


import com.ecabs.common.dto.BookingDTO;

public interface BookingProducerService {
    void sendBookingAddMessage(BookingDTO bookingDTO);
    void sendBookingEditMessage(BookingDTO bookingDTO);
    void sendBookingDeleteMessage(Long bookingId);
}
