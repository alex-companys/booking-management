package com.ecabs.bookingmanagement.service;


import com.ecabs.common.dto.BookingDTO;
import com.ecabs.common.model.Booking;

import java.util.List;

public interface BookingService {

    BookingDTO createBooking(BookingDTO booking);

    BookingDTO updateBooking(BookingDTO updatedBookingDTO);

    void deleteBooking(Long bookingId);

    Booking getBooking(Long bookingId);

    List<Booking> getAllBookings();
}

