package com.ecabs.common.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Represents a booking entity for a transportation service.
 * This class defines the details of a booking, including the passenger information,
 * pickup details, number of passengers, pricing, and related trip waypoints.
 * <p>
 * The 'asap' field is a boolean flag indicating whether the booking is requested
 * to be processed as soon as possible, without any significant delay.
 * <p>
 * This entity is mapped to a corresponding database table using JPA annotations,
 * and it includes relationships with the 'TripWaypoint' class to model multiple
 * stopping points during a booking.
 * <p>
 * The class provides constructors for creating booking instances with and without
 * specified attributes. It also includes getter and setter methods for accessing
 * and modifying the booking details.
 */

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Booking implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "passenger_name", nullable = false)
    private String passengerName;

    @Column(name = "passenger_contact_number", nullable = false)
    private String passengerContactNumber;

    @Column(name = "pickup_time")
    private LocalDateTime pickupTime;

    @Column(name = "asap", nullable = false)
    private boolean asap;

    @Column(name = "waiting_time")
    private int waitingTime;

    @Column(name = "number_of_passengers", nullable = false)
    private int numberOfPassengers;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "rating")
    private double rating;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "last_modified_on")
    private LocalDateTime lastModifiedOn;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "booking")
    @ToString.Exclude
    private List<TripWaypoint> tripWaypoints;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return Objects.equals(id, booking.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
