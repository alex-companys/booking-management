package com.ecabs.common.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.Objects;

/**
 * Represents a stopping point during a booking, capturing location details
 * such as locality name, latitude, and longitude.
 * <p>
 * The 'locality' field stores the name of the location where the waypoint is situated.
 * The 'latitude' and 'longitude' fields represent the geographical coordinates of the location.
 * <p>
 * This class is mapped to a corresponding database table using JPA annotations, and it
 * includes a many-to-one relationship with the 'Booking' class, associating each waypoint
 * with a specific booking.
 * <p>
 * The class provides constructors for creating waypoint instances with and without
 * specified attributes. It also includes getter and setter methods for accessing
 * and modifying the waypoint details.
 */

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TripWaypoint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "locality", nullable = false)
    private String locality;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    @Column(name = "longitude", nullable = false)
    private double longitude;

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private Booking booking;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TripWaypoint that = (TripWaypoint) o;
        return id != null && Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
