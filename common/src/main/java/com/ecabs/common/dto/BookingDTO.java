package com.ecabs.common.dto;

import com.ecabs.common.model.TripWaypoint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
public class BookingDTO implements Serializable {

    private Long id;
    private String passengerName;
    private String passengerContactNumber;
    private LocalDateTime pickupTime;
    private boolean asap;
    private int waitingTime;
    private int numberOfPassengers;
    private double price;
    private double rating;
    private List<TripWaypoint> tripWaypoints;

}
