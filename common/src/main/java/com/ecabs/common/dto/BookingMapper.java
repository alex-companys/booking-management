package com.ecabs.common.dto;

import com.ecabs.common.model.Booking;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class BookingMapper {
    private final ModelMapper modelMapper;

    public BookingMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Booking mapDTOtoEntity(BookingDTO bookingDTO) {
        return modelMapper.map(bookingDTO, Booking.class);
    }

    public BookingDTO mapEntityToDTO(Booking booking) {
        return modelMapper.map(booking, BookingDTO.class);
    }

}
