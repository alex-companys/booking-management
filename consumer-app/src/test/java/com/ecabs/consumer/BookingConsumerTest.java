package com.ecabs.consumer;

import com.ecabs.common.dto.BookingDTO;
import com.ecabs.common.dto.BookingMapper;
import com.ecabs.common.model.Booking;
import com.ecabs.common.model.TripWaypoint;
import com.ecabs.consumer.repository.BookingRepository;
import com.ecabs.consumer.service.BookingConsumer;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.core.Message;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(SpringExtension.class)
class BookingConsumerTest {

    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private BookingMapper bookingMapper;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private BookingConsumer bookingConsumer;

    @Test
    void receiveMessageShouldLogMessage() {
        Message message = mock(Message.class);
        when(message.getBody()).thenReturn("test message".getBytes());
        BookingConsumer bookingConsumerMock = mock(BookingConsumer.class);

        bookingConsumerMock.receiveMessage(message);
        verify(bookingConsumerMock, times(1)).receiveMessage(message);
    }

    @Test
    void processAddBookingShouldSaveBooking() throws IOException {
        Message message = mock(Message.class);
        when(message.getBody()).thenReturn("123".getBytes());

        BookingDTO bookingDTO = new BookingDTO();
        Booking booking = new Booking();
        booking.setTripWaypoints(Collections.singletonList(new TripWaypoint()));

        when(objectMapper.readValue(any(byte[].class), eq(BookingDTO.class))).thenReturn(bookingDTO);
        when(bookingMapper.mapDTOtoEntity(bookingDTO)).thenReturn(booking);

        bookingConsumer.processAddBooking(message);
        verify(bookingRepository).save(booking);
    }

    @Test
    void processEditBookingShouldUpdateBooking() throws IOException {
        Message message = mock(Message.class);
        when(message.getBody()).thenReturn("123".getBytes());

        BookingDTO bookingDTO = new BookingDTO();
        bookingDTO.setId(1L);

        Booking existingBooking = new Booking();
        existingBooking.setId(bookingDTO.getId());
        when(objectMapper.readValue(any(byte[].class), eq(BookingDTO.class))).thenReturn(bookingDTO);
        when(bookingRepository.existsById(1L)).thenReturn(true);
        when(bookingMapper.mapDTOtoEntity(bookingDTO)).thenReturn(existingBooking);
        when(bookingRepository.save(existingBooking)).thenReturn(existingBooking);

        bookingConsumer.processEditBooking(message);
        assertNotNull(existingBooking.getLastModifiedOn());
        assertEquals(existingBooking.getId(), bookingDTO.getId());
        verify(bookingRepository, times(1)).save(existingBooking);
    }

    @Test
    void processDeleteBookingShouldDeleteBooking() {
        Message message = mock(Message.class);
        when(message.getBody()).thenReturn("123".getBytes());

        when(bookingRepository.existsById(anyLong())).thenReturn(true);
        doNothing().when(bookingRepository).deleteById(anyLong());

        bookingConsumer.processDeleteBooking(message);
        verify(bookingRepository, times(1)).deleteById(anyLong());
    }

    @Test
    void processDeleteBookingShouldHandleEmptyResultDataAccessException() {
        Message message = mock(Message.class);

        when(message.getBody()).thenReturn("1".getBytes());
        doThrow(new EmptyResultDataAccessException(1)).when(bookingRepository).deleteById(1L);

        bookingConsumer.processDeleteBooking(message);
        assertThrows(EmptyResultDataAccessException.class, () -> bookingRepository.deleteById(1L));
    }
}

