package com.ecabs.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.ecabs.common", "com.ecabs.consumer"})
@EntityScan(basePackages = {"com.ecabs.common"})
public class ConsumerAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerAppApplication.class, args);
    }

}
