package com.ecabs.consumer.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class RabbitMQConsumerConfig {

    @Value("${rabbitmq.message.exchange.name}")
    private String exchangeName;

    @Value("${rabbitmq.audit.queue}")
    private String auditQueueName;

    @Value("${rabbitmq.booking.exchange.name}")
    private String bookingExchangeName;

    @Value("${rabbitmq.add.queue.name}")
    private String addQueueName;

    @Value("${rabbitmq.edit.queue.name}")
    private String editQueueName;

    @Value("${rabbitmq.delete.queue.name}")
    private String deleteQueueName;

    @Value("${rabbitmq.routingkey.add}")
    private String bookingAddRoutingKey;

    @Value("${rabbitmq.routingkey.edit}")
    private String bookingEditRoutingKey;

    @Value("${rabbitmq.routingkey.delete}")
    private String bookingDeleteRoutingKey;

    @Value("${rabbitmq.routingkey.audit}")
    private String bookingAuditRoutingKey;

    @Bean
    public DirectExchange bookingExchange() {
        return new DirectExchange(bookingExchangeName);
    }

    @Bean
    public Queue auditQueue() {
        return new Queue(auditQueueName);
    }

    @Bean
    public Queue addQueue() {
        return new Queue(addQueueName);
    }

    @Bean
    public Queue editQueue() {
        return new Queue(editQueueName);
    }

    @Bean
    public Queue deleteQueue() {
        return new Queue(deleteQueueName);
    }

    @Bean
    public TopicExchange messageExchange() {
        return new TopicExchange(exchangeName);
    }

    @Bean
    public Binding addBinding(Queue addQueue, DirectExchange bookingExchange) {
        return BindingBuilder.bind(addQueue).to(bookingExchange).with(bookingAddRoutingKey);
    }

    @Bean
    public Binding editBinding(Queue editQueue, DirectExchange bookingExchange) {
        return BindingBuilder.bind(editQueue).to(bookingExchange).with(bookingEditRoutingKey);
    }

    @Bean
    public Binding deleteBinding(Queue deleteQueue, DirectExchange bookingExchange) {
        return BindingBuilder.bind(deleteQueue).to(bookingExchange).with(bookingDeleteRoutingKey);
    }

    @Bean
    public Binding auditBinding(Queue auditQueue, TopicExchange messageExchange) {
        return BindingBuilder.bind(auditQueue).to(messageExchange).with(bookingAuditRoutingKey);
    }

    @Bean
    public Binding messageToBookingExchangeBinding(DirectExchange bookingExchange, TopicExchange messageExchange) {
        return BindingBuilder.bind(bookingExchange).to(messageExchange).with(bookingAuditRoutingKey);
    }

    @Bean
    public ObjectMapper mapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        return mapper;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter(ObjectMapper mapper) {
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, Jackson2JsonMessageConverter jackson2JsonMessageConverter) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jackson2JsonMessageConverter);
        return rabbitTemplate;
    }
}
