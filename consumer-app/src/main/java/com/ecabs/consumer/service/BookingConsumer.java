package com.ecabs.consumer.service;

import com.ecabs.common.dto.BookingDTO;
import com.ecabs.common.dto.BookingMapper;
import com.ecabs.common.model.Booking;
import com.ecabs.common.model.TripWaypoint;
import com.ecabs.consumer.repository.BookingRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Consumer class responsible for handling messages from RabbitMQ queues related to booking operations.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class BookingConsumer {

    private final BookingRepository bookingRepository;
    private final BookingMapper bookingMapper;
    private final ObjectMapper objectMapper;

    @RabbitListener(queues = "${rabbitmq.audit.queue}")
    public void receiveMessage(Message message) {
        log.info("Received message from MessageAuditQueue: " + new String(message.getBody()));
    }

    @RabbitListener(queues = "${rabbitmq.add.queue.name}")
    public void processAddBooking(Message message) throws IOException {
        BookingDTO bookingDTO = objectMapper.readValue(message.getBody(), BookingDTO.class);
        Booking booking = processBookingDTO(bookingDTO);
        bookingRepository.save(booking);

        log.info("Received message from 'Add' queue: {}", new String(message.getBody()));
    }

    @RabbitListener(queues = "${rabbitmq.edit.queue.name}")
    public void processEditBooking(Message message) throws IOException {
        BookingDTO bookingDTO = objectMapper.readValue(message.getBody(), BookingDTO.class);
        Long id = bookingDTO.getId();
        if (!bookingRepository.existsById(id)) {
            throw new EntityNotFoundException("Booking not found with ID: " + id);
        }
        Booking updatedBooking = processBookingDTO(bookingDTO);
        Booking savedBooking = bookingRepository.save(updatedBooking);

        log.info("Received message from 'Edit' queue: {}", savedBooking);
    }

    @RabbitListener(queues = "${rabbitmq.delete.queue.name}")
    public void processDeleteBooking(Message message) {
        Long bookingId = Long.valueOf(new String(message.getBody()));
        try {
            bookingRepository.deleteById(bookingId);
            log.info("Booking deleted with ID: {}", bookingId);
        } catch (EmptyResultDataAccessException ex) {
            log.info("Booking not found with ID: {}", bookingId);
            log.info("Received message from 'Delete' queue: {}", bookingId);
        }
    }

    private Booking processBookingDTO(BookingDTO bookingDTO) {
        return getBookingFormDTO(bookingDTO);
    }

    private Booking getBookingFormDTO(BookingDTO bookingDTO) {
        Booking updatedBooking = bookingMapper.mapDTOtoEntity(bookingDTO);
        updatedBooking.setCreatedOn(LocalDateTime.now());
        updatedBooking.setLastModifiedOn(LocalDateTime.now());

        List<TripWaypoint> tripWaypoints = bookingDTO.getTripWaypoints();
        if (tripWaypoints != null) {
            for (TripWaypoint tripWaypoint : tripWaypoints) {
                tripWaypoint.setBooking(updatedBooking);
            }
        }
        updatedBooking.setTripWaypoints(tripWaypoints);
        return updatedBooking;
    }

}
