# Booking Management Application

This application manages bookings, providing functionalities for creating, updating, deleting, and retrieving booking details.
It interacts with a database for storage and RabbitMQ for message publishing.

The project consists of three services:
consumer-app: This service contains the consumer logic.
common: This module includes shared libraries and utilities.
booking-management: This service functions as a Publisher, responsible for publishing messages.
## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Setting Up the Database](#setting-up-the-database)
    - [Running the Application](#running-the-application)
- [API Documentation](#api-documentation)
    - [Swagger UI](#swagger-api)    
- [Docker Compose](#docker-compose)
- [Contributing](#contributing)
- [License](#license)

## Features

- Create Booking: Adds a new booking and publishes messages to RabbitMQ for auditing and processing.

- Update Booking: Modifies an existing booking and publishes messages to RabbitMQ for auditing and processing.

- Delete Booking: Removes a booking by its ID and publishes messages to RabbitMQ for auditing and processing.

- Get Booking: Retrieves details of a specific booking by its ID.

- Get All Bookings: Retrieves details of all bookings.

## Getting Started

### Prerequisites

Before running the application, make sure you have the following prerequisites installed:

- Java Development Kit (JDK) 17 or higher
- Maven
- Docker
- PostgreSQL
- RabbitMQ

### Setting Up the Database
- Create a PostgreSQL database named booking_management.

- Configure the database connection in application.properties:

- This application uses a PostgreSQL database. You can set up the database using the provided `docker-compose.yaml` file. To start the database, run the following command in the project directory:

```bash
docker-compose up
```
### Setting Up RabbitMQ

- Install RabbitMQ and start the RabbitMQ server.
- Configure RabbitMQ connection details in application.properties

### Running the Application

1. Clone the repository.
2. Build the application using Maven:

 ```bash
   mvn clean install
   ```

3. Start the application:
```bash
mvn spring-boot:run
```

4. The application will be available at `http://localhost:8080`.


## Docker Compose

A `docker-compose.yaml` file is provided to configure the PostgreSQL database and RabbitMQ message broker. To start the database and RabbitMQ broker, you need to go to the folder where the `docker-compose.yaml` file is stored and
run the following command:

```shell
docker-compose up
```
## Contributing
Contributions are welcome! Feel free to submit issues and pull requests.

## License
This project is licensed under the MIT License.

Note: The initial conditions and requirements for this application were provided in a [PDF document](eCabs_-_Backend_Technical_Task.pdf). Please refer to the document for additional details on the project.